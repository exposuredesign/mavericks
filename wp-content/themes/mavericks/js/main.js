/*
  // Project: Mavericks 80's Lounge
  // Designer: Para Ltd
  // Design Copyright: Para Ltd
  // Developer: Adam Wadsworth
  // Development Copyright: Adam Wadsworth
*/
$(document).foundation();
$(function() {
  function slider() {
    $(".mslider").slick({
      dots: false,
      arrows: false,
      infinite: true,
      autoplay: true,
      speed: 500,
      fade: true,
      cssEase: 'linear'
    });
  }

  slider();

  $(".fancybox").fancybox({
    openEffect	: 'none',
    closeEffect	: 'none'
  });
});
