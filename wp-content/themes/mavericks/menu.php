<?php
  // Template Name: Menu
  // Project: Mavericks 80's Lounge
  // Designer: Para Ltd
  // Design Copyright: Para Ltd
  // Developer: Adam Wadsworth
  // Development Copyright: Adam Wadsworth
  get_header();
?>

<?php $hero = get_field('hero'); if($hero) { ?>
<div id="hero">
  <img alt="" src="<?php echo $hero; ?>" style="width:100%;">
</div>
<?php } ?>

<div class="expanded" id="main">
  <?php while ( have_posts() ) : the_post(); ?>
  <div class="expanded">
    <div class="row">
      <div class="column text-center">
        <h1><?php the_title(); ?></h1>
      </div>
    </div>
  </div>
  <?php endwhile;?>
  <?php if( have_rows('blocks') ): ?>
    <?php $number = 0 ?>
    <?php while( have_rows('blocks') ): the_row(); $title = get_sub_field('title'); $description = get_sub_field('description'); ?>
      <?php $number++ ?>
      <div class="expanded pad" style="<?php $even = array(0, 2, 4, 6, 8); if(in_array(substr($number, -1),$even)){ ?>background-color:#e8e8e8;<?php }?>">
        <div class="row">
          <div class="column">

            <h2 <?php if($description) { echo 'style="margin-bottom:10px;"'; } ?>><?php echo $title; ?></h2>
            <?php if($description) { echo '<p>'.$description.'</p>'; } ?>

          </div>
        </div>
        <div class="row">
          <?php if( have_rows('block') ): ?>
            <?php while( have_rows('block') ): the_row(); $title = get_sub_field('title'); $content = get_sub_field('content'); $price = get_sub_field('price'); ?>
              <div class="columns small-12 medium-4 small-4">
                <h3><?php echo $title;?></h3>
                <p><?php echo $content;?></p>
                <p>
                  <span><?php echo $price;?></span>
                </p>
              </div>
            <?php endwhile; ?>
          <?php endif; ?>
        </div>
      </div>
    <?php endwhile; ?>
  <?php endif; ?>

  <?php $type = get_field('type');
  if($type == 'Huddersfield') { ?>
  <div class="expanded" id="gallery-booth">
    <div class="row">
      <div class="columns small-12 medium-6 large-6">
        <h4 style="color: #110974;; font-family: 'reforma'; text-transform: uppercase; font-size: 40px; margin: 0; padding: 0; display: block;">Having a Party?</h4>
        <p>Let us take the headache away from planning your big day, be it birthday celebration, leaving party, or wedding reception – we know how to get a party started. You will have full access to our Party Planner who will take care of your every need, leaving you to sit back, relax and party!!!</p>
      </div>
      <div class="columns small-12 medium-6 large-6 box">
        <div class="book-a-booth" style="background: url(<?php echo home_url(); ?>/wp-content/uploads/2016/09/HuddsBooth-box-bg-762x481.jpg) no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
          <div class="row align-middle">
            <div class="column text-center">
              <span class="title">BOOK A BOOTH</span>
              <span class="content">There’s no party like a Maverick’s party!</span>
              <a href="http://mavericks80slounge.co.uk/huddersfield/huddersfield-book-booth/" class="button">More</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php } ?>
  <?php if($type == 'Horsforth') { ?>
    <div class="expanded" id="gallery-booth">
      <div class="row">
        <div class="columns small-12 medium-6 large-6">
          <h4 style="color: #110974; font-family: 'reforma'; text-transform: uppercase; font-size: 40px; margin: 0; padding: 0; display: block;">Having a Party?</h4>
          <p>Let us take the headache away from planning your big day, be it birthday celebration, leaving party, or wedding reception – we know how to get a party started. You will have full access to our Party Planner who will take care of your every need, leaving you to sit back, relax and party!!!</p>
        </div>
        <div class="columns small-12 medium-6 large-6 box">
          <div class="book-a-booth" style="background: url(<?php echo home_url(); ?>/wp-content/uploads/2016/09/HuddsBooth-box-bg-762x481.jpg) no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
            <div class="row align-middle">
              <div class="column text-center">
                <span class="title">BOOK A BOOTH</span>
                <span class="content">There’s no party like a Maverick’s party!</span>
                <a href="http://mavericks80slounge.co.uk/horsforth/horsforth-book-booth/" class="button">More</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php } ?>
  <?php if($type == 'Bingley') { ?>
    <div class="expanded" id="gallery-booth">
      <div class="row">
        <div class="columns small-12 medium-6 large-6">
          <h4 style="color: #110974; font-family: 'reforma'; text-transform: uppercase; font-size: 40px; margin: 0; padding: 0; display: block;">Having a Party?</h4>
          <p>Let us take the headache away from planning your big day, be it birthday celebration, leaving party, or wedding reception – we know how to get a party started. You will have full access to our Party Planner who will take care of your every need, leaving you to sit back, relax and party!!!</p>
        </div>
        <div class="columns small-12 medium-6 large-6 box">
          <div class="book-a-booth" style="background: url(<?php echo home_url(); ?>/wp-content/uploads/2016/09/HuddsBooth-box-bg-762x481.jpg) no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
            <div class="row align-middle">
              <div class="column text-center">
                <span class="title">BOOK A BOOTH</span>
                <span class="content">There’s no party like a Maverick’s party!</span>
                <a href="http://mavericks80slounge.co.uk/bingley/bingley-book-booth/" class="button">More</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php } ?>
</div>
<?php get_footer(); ?>
