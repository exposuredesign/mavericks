<?php
  // Template Name: Venue
  // Project: Mavericks 80's Lounge
  // Designer: Para Ltd
  // Design Copyright: Para Ltd
  // Developer: Adam Wadsworth
  // Development Copyright: Adam Wadsworth
  get_header();
?>

<?php if( have_rows('hero_slider') ): ?>
<div class="expanded" id="slider">
  <div class="mslider">
    <?php while( have_rows('hero_slider') ): the_row(); $overlay = get_sub_field('overlay'); $media_link = get_sub_field('media_link');  $background = get_sub_field('background'); $link = get_sub_field('link'); ?>
      <div class="item" style="background: url(<?php echo $background; ?>) no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
        <div class="row align-middle">
          <div class="column text-center">
            <?php if($media_link) {?>
              <a href="<?php echo $media_link; ?>">
                <img src="<?php echo $overlay; ?>" style="margin:0px auto;">
              </a>
            <?php } elseif($link) { ?>
              <a href="<?php echo $link; ?>">
                <img src="<?php echo $overlay; ?>" style="margin:0px auto;">
              </a>
            <?php } ?>
          </div>
        </div>
      </div>
    <?php endwhile; ?>
  </div>
</div>
<?php endif; ?>

<div class="expanded" id="main" style="padding:0px 0px 20px 0px;">

  <?php if( have_rows('banner') ): ?>
    <?php while( have_rows('banner') ): the_row(); $background = get_sub_field('background'); $banner_link = get_sub_field('banner_link'); ?>
      <div class="row venue">
        <div class="column">
          <a class="banner" href="<?php echo $banner_link; ?>" title="#">
            <img alt="#" src="<?php echo $background; ?>"/>
          </a>
        </div>
      </div>
    <?php endwhile; ?>
  <?php endif; ?>

  <div class="expanded">
    <div class="row items" data-equalizer>
      <?php if( have_rows('blocks') ): ?>
        <?php while( have_rows('blocks') ): the_row();
          $title = get_sub_field('title');
          $content = get_sub_field('content');
          $link_title_1 = get_sub_field('link_title_1');
          $link_url_1 = get_sub_field('link');
          $link_title_2 = get_sub_field('link_title_2');
          $link_url_2 = get_sub_field('link_url_2');
          $background = get_sub_field('background');
        ?>
          <div class="columns small-12 medium-6 large-6 item">
            <div class="text-center" data-equalizer-watch style="background: url(<?php echo $background; ?>) no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
              <h2><?php echo $title; ?></h2>
              <p><?php echo $content; ?></p>
              <?php if($link_title_1) { ?><a class="button" href="<?php echo $link_url_1; ?>"><?php echo $link_title_1; ?></a><?php } ?>
              <?php if($link_title_2) { ?><a class="button" href="<?php echo $link_url_2; ?>"><?php echo $link_title_2; ?></a><?php } ?>
            </div>
          </div>
        <?php endwhile; ?>
      <?php endif; ?>
    </div>
  </div>

  
  <div class="expanded" id="gallery-booth" style="margin:20px 0px 0px 0px;">
    <div class="row">
      <div class="columns small-12 medium-6 large-6">
       <?php 
          while( have_rows('find_us') ): the_row(); 
          $title = get_sub_field('title');
          $content = get_sub_field('content'); 
          $map = get_sub_field('map'); 
        ?>
        <h4 class="find-us"style=""><?php echo $title;?></h4>
        
        <?php echo $content;?>
<!--
     <p>Maverick’s is a two floor venue located on 32 King Street, Huddersfield (next to Subways) – opposite the entrance to Kingsgate Shopping Centre</p>
        <p><span style="color:#2ba6cb;">Address:</span> 32 King Street, Huddersfield<br>HD2 2QT</p>
        <p><span style="color:#2ba6cb;">Contact us:</span> 01484 531 999<br><a href="mailto:huddersfield@mavericks80slounge.co.uk">huddersfield@mavericks80slounge.co.uk</a></p>
        -->
      </div>
      <div class="columns small-12 medium-6 large-6 box" style="padding:0px; margin:0px;">
        <div class="book-a-booth" style="height:auto;">
          <div class="flex-video widescreen">
            <!--
            <iframe allowfullscreen="" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d9460.206376046415!2d-1.7792264!3d53.6460522!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7a09afeac34405e8!2sMaverick's+80s+Lounge!5e0!3m2!1sen!2suk!4v1467651002397" style="border:0" frameborder="0" height="300" width="100"></iframe>
            -->
            <iframe allowfullscreen="" src="<?php echo $map;?>" style="border:0" frameborder="0" height="300" width="100"></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endwhile; ?>
  
</div>
<?php
  get_footer();
?>
