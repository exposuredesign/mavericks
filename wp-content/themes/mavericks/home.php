<?php
  // Template Name: Home
  // Project: Mavericks 80's Lounge
  // Designer: Para Ltd
  // Design Copyright: Para Ltd
  // Developer: Adam Wadsworth
  // Development Copyright: Adam Wadsworth
  get_header();
?>

<?php if( have_rows('hero_slider') ): ?>
<div class="expanded" id="slider">
  <div class="mslider">
    <?php while( have_rows('hero_slider') ): the_row(); $overlay = get_sub_field('overlay'); $media_link = get_sub_field('media_link'); $background = get_sub_field('background'); $link = get_sub_field('link'); ?>
      <div class="item" style="background: url(<?php echo $background; ?>) no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
        <div class="row align-middle">
          <div class="column text-center">
            <?php if($media_link) {?>
              <a href="<?php echo $media_link; ?>">
                <img src="<?php echo $overlay; ?>" style="margin:0px auto;">
              </a>
            <?php } elseif($link) { ?>
              <a href="<?php echo $link; ?>">
                <img src="<?php echo $overlay; ?>" style="margin:0px auto;">
              </a>
            <?php } ?>
          </div>
        </div>
      </div>
    <?php endwhile; ?>
  </div>
</div>
<?php endif; ?>

<div class="expanded" id="main">
  <div class="mavericks-tv">
    <div class="row">
      <div class="column">
        <div class="title text-center">
          <a class="mavericks-logo" href="#" title="Mavericks TV">
            <img alt="Mavericks TV" src="<?php echo get_template_directory_uri(); ?>/svgs/mavericks-tv.svg"/>
          </a>
        </div>

        <div class="video">
          <div class="tabs-content" data-tabs-content="video-tabs">
            <?php if( have_rows('mavericks_tv') ): ?>
              <?php
                $c = 0;
                while( have_rows('mavericks_tv') ): the_row();
                $youtube_link = get_sub_field('youtube_link');
                $c ++;
              ?>
              <div class="tabs-panel <?php if($c === 1){ ?>is-active<?php } ?>" id="video-<?php echo $c ++; ?>">
                <div class="flex-video widescreen">
                  <iframe width="994" height="512" src="<?php echo $youtube_link;?>?modestbranding=1&autohide=1&showinfo=0" frameborder="0" allowfullscreen></iframe>
                </div>
              </div>
              <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>

        <div class="videos">
          <ul class="tabs" data-tabs id="video-tabs">
            <?php if( have_rows('mavericks_tv') ): ?>
              <?php
                $v = 0;
                while( have_rows('mavericks_tv') ): the_row();
                $thumbnail = get_sub_field('thumbnail');
                $v ++;
              ?>
              <li class="tabs-title <?php if($v === 1){ ?>is-active<?php } ?>">
                <a aria-selected="true" href="#video-<?php echo $v ++; ?>" title="">
                  <img src="<?php echo $thumbnail; ?>">
                </a>
              </li>
              <?php endwhile; ?>
            <?php endif; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <?php if( have_rows('mavericks_on_tour') ): ?>
    <?php while( have_rows('mavericks_on_tour') ): the_row();
      $title = get_sub_field('title');
      $content = get_sub_field('content');
      $background = get_sub_field('background');
      $link = get_sub_field('link');
    ?>
    <div class="mavericks-on-tour">
      <div class="row align-middle" style="background: url(<?php echo $background; ?>) no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
        <div class="column text-center">
          <h4 class="text-center"><?php echo $title; ?></h4>
          <p class="text-center"><?php echo $content; ?></p>
          <a class="button" href="<?php echo $link; ?>" title="<?php echo $title; ?>">More</a>
        </div>
      </div>
    </div>
    <?php endwhile; ?>
  <?php endif; ?>

  <div class="venues">
    <div class="one row text-center">
      <div class="column">
        <div class="title">
          <h3>Venues</h3>
        </div>
        <div class="two row">
        <?php if( have_rows('venues') ): ?>
          <?php
            while( have_rows('venues') ): the_row();
            $title = get_sub_field('title');
            $link = get_sub_field('link');
            $background = get_sub_field('background');
          ?>
          <div class="columns small-12 medium-4 large-4" style="background: url(<?php echo $background; ?>) no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
            <?php if($link) { ?><a href="<?php echo $link; ?>" title=""><?php } ?>
              <div class="row align-middle">
                <div class="column">
                  <span class="one"><?php echo $title; ?></span>
                    <?php if($link) { ?><span class="two">More</span><?php } ?>
                </div>
              </div>
            <?php if($link) { ?></a><?php } ?>
          </div>
          <?php endwhile; ?>
        <?php endif; ?>
        </div>
      </div>
    </div>
  </div>

  <div class="sign-up">
      <div class="row">
        <div class="columns small-12 medium-12 large-6 text-center medium-text-center large-text-left">
          <h5>Stay in the know</h5>
          <p>Sign up to hear about<br>
            <strong>Mavericks</strong>
            news and offers</p>
        </div>
        <div class="columns small-12 medium-12 large-6">
          <?php echo do_shortcode('[mc4wp_form id="392"]');?>
        </div>
      </div>
    </div>
</div>

<?php
  get_footer();
?>
