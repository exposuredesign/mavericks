<?php
  // Template Name: Join Our Team
  // Project: Mavericks 80's Lounge
  // Designer: Para Ltd
  // Design Copyright: Para Ltd
  // Developer: Adam Wadsworth
  // Development Copyright: Adam Wadsworth
  get_header();
?>

<?php $hero = get_field('hero'); if($hero) { ?>
<div class="" id="hero">
  <img alt="" src="<?php echo $hero; ?>" style="width:100%;">
</div>
<?php } ?>

<div class="expanded" id="main">

  <div class="row">
    <div class="columns small-12 medium-8 large-8">
      <?php while ( have_posts() ) : the_post(); ?>
        <h1 style="margin-left:0px; padding-left:0px;"><?php the_title(); ?></h1>
        <?php the_content(); ?>
      <?php endwhile;?>
      <?php echo do_shortcode('[contact-form-7 id="203" title="Join our Team"]'); ?>
    </div>
    <div class="columns small-12 medium-4 large-4 sidebar" style="margin:0px 0px 40px 0px;">
          <h2>Find us</h2>
          <div class="textwidget" style="margin:0px 0px 20px 0px; padding:0px 0px 0px 0px;">
            <p>
              Please see all our contact details below:
            </p>
            <p>
              <strong>Huddersfield</strong><br>
              <span>Address:</span> 32 King Street, Huddersfield,<br> HD2 2QT
            </p>
            <p>
              <span>Contact us:</span> 01484 531 999<br><a href="mailto:huddersfield@mavericks80slounge.co.uk">huddersfield@mavericks80slounge.co.uk</a>
            </p>
          </div>
          <div class="textwidget" style="margin:0px 0px 20px 0px; padding:0px 0px 0px 0px;">
            <p>
              <strong>Horsforth</strong><br>
              <span>Address:</span> 62 Town Street, Horsforth,<br>Leeds, LS18 4AP
            </p>
            <p>
              <span>Contact us:</span> 01132 588 599<br><a href="mailto:horsforth@mavericks80slounge.co.uk">horsforth@mavericks80slounge.co.uk</a>
            </p>
          </div>

          <div class="textwidget" style="margin:0px 0px 20px 0px; padding:0px 0px 0px 0px;">
            <p>
              <strong>Bingley</strong><br/>
              <span>Address:</span> 148 Main Street, Bingley,<br/>BD16 2HL
            </p>
            <p><span>Contact us:</span> 01274 569299<br> <a href="mailto:bingley@mavericks80slounge.co.uk">bingley@mavericks80slounge.co.uk</a></p>
       </div>

      </div>
  </div>

</div>

<?php
  get_footer();
?>
