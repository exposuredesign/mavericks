<?php
  // Project: Mavericks 80's Lounge
  // Designer: Para Ltd
  // Design Copyright: Para Ltd
  // Developer: Adam Wadsworth
  // Development Copyright: Adam Wadsworth
  get_header();
?>

<?php
  $queried_object = get_queried_object();
  $taxonomy = $queried_object->taxonomy;
  $term_id = $queried_object->term_id;
  $hero = get_field('hero', $queried_object);
  if($hero) {
?>
<div id="hero">
  <img alt="" src="<?php echo $hero; ?>" style="width:100%;">
</div>
<?php } ?>

<div class="expanded" id="main">

  <div class="row">
    <div class="columns small-12 medium-8 large-8 posts">
      <h1>What's on <?php echo single_cat_title( '', false );?></h1>
      <?php if ( have_posts() ) { ?>
        <?php while ( have_posts() ) : the_post(); ?>
          <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' ); $url = $thumb['0']; ?>
          <?php if($url){ ?>
            <div class="row post">
              <div class="column">
                <h3 style="float: left; width: 100%; color: rgb(48, 172, 223); font-family: 'reforma'; text-transform: uppercase; font-size: 40px; line-height: 30px; margin: 0 0 10px 0; padding: 0; display: block;"><?php the_title(); ?></h3>
                <?php echo wp_get_attachment_image(get_post_thumbnail_id($post->ID), 'single'); ?>
                <div style="width:100%; flaot:left; height:20px;"></div>
                <?php the_content(); ?>
              </div>
            </div>
          <?php } else { ?>
            <div class="row post">
              <div class="column">
                <h3 style="float: left; width: 100%; color: rgb(48, 172, 223); font-family: 'reforma'; text-transform: uppercase; font-size: 40px; line-height: 30px; margin: 0 0 10px 0; padding: 0; display: block;"><?php the_title(); ?></h3>
                <div style="width:100%; flaot:left; height:20px;"></div>
                <?php the_content(); ?>
              </div>
            </div>
          <?php } ?>
        <?php endwhile; the_posts_pagination(); } else { ?>
        <div class="row post">
          <div class="column">
            <a href="#" alt="<?php the_title(); ?>" class="title">
              <h3>No Events</h3>
            </a>
          </div>
        </div>
      <?php } ?>
    </div>

    <?php
      $queried_object = get_queried_object();
      $taxonomy = $queried_object->taxonomy;
      $term_id = $queried_object->term_id;
      $cat_type = get_field('type', $queried_object);
     ?>
    <?php if($cat_type == 'Huddersfield') { ?>
    <div class="columns small-12 medium-4 large-4 sidebar">
      <div class="textwidget">
        <a href="<?php echo home_url(); ?>/huddersfield/huddersfield-book-booth/" style="width:100%; float:left; margin:15px 0px 0px 0px;" class="book">
          <img src="<?php echo home_url(); ?>/wp-content/uploads/2016/09/Book-a-booth-1.jpg" style="width:100%; height:auto;">
        </a>
      </div>
      <h2>Find us</h2>
      <div class="textwidget">
        <p>Maverick’s is a two floor venue located on 32 King Street, Huddersfield (next to Subways) – opposite the entrance to Kingsgate Shopping Centre</p>
        <p></p>
        <p><span>Address:</span> 32 King Street, Huddersfield<br>HD2 2QT</p>
        <p><span>Contact us:</span> 01484 531 999<br><a href="mailto:huddersfield@mavericks80slounge.co.uk">huddersfield@mavericks80slounge.co.uk</a></p>
      </div>
    	<h2>Location</h2>
      <div class="textwidget">
        <div class="flex-video">
          <iframe allowfullscreen="" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d9460.206376046415!2d-1.7792264!3d53.6460522!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7a09afeac34405e8!2sMaverick's+80s+Lounge!5e0!3m2!1sen!2suk!4v1467651002397" style="border:0" frameborder="0" height="450" width="600"></iframe>
        </div>
      </div>
    </div>
    <?php } ?>
    <?php if($cat_type == 'Horsforth') { ?>
    <div class="columns small-12 medium-4 large-4 sidebar">
      <div class="textwidget">
        <a href="<?php echo home_url(); ?>/horsforth/horsforth-book-booth/" style="width:100%; float:left; margin:15px 0px 0px 0px;" class="book">
          <img src="<?php echo home_url(); ?>/wp-content/uploads/2016/09/Book-a-booth-1.jpg" style="width:100%; height:auto;">
        </a>
      </div>
      <h2>Find us</h2>
      <div class="textwidget">
        <p>Maverick’s is located on 62 Town Street, Horsforth – diagonally opposite the entrance to Morrisons Supermarket.</p>
        <p><span>Address:</span> 62 Town Street, Horsforth, Leeds<br> LS18 4AP</p>
        <p><span>Contact us:</span> 01132 588 599<br> <a href="mailto:horsforth@mavericks80slounge.co.uk">horsforth@mavericks80slounge.co.uk</a></p>
      </div>
      <h2>Location</h2>
      <div class="textwidget"><div class="flex-video">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2354.2527985860193!2d-1.6420337841429298!3d53.83836748008502!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487958969829efbd%3A0x92c2e186faf02df3!2s62+Town+St%2C+Horsforth%2C+Leeds%2C+West+Yorkshire+LS18!5e0!3m2!1sen!2suk!4v1467660906500" style="border:0" allowfullscreen="" frameborder="0" height="450" width="600"></iframe>
      </div>
    </div>
	</div>
  <?php } ?>
  <?php if($cat_type == 'Bingley') { ?>
  <div class="columns small-12 medium-4 large-4 sidebar">
    <div class="textwidget">
      <a href="<?php echo home_url(); ?>/bingley/bingley-book-booth/" style="width:100%; float:left; margin:15px 0px 0px 0px;" class="book">
        <img src="<?php echo home_url(); ?>/wp-content/uploads/2016/09/Book-a-booth-1.jpg" style="width:100%; height:auto;">
      </a>
    </div>
    <h2>Find us</h2>
    <div class="textwidget">
      <p>Maverick's is located on Main Street (formerly The Midland Hotel) in Bingley.</p>
      <p><span>Address:</span> 148 Main Street, Bingley, <BR/>BD16 2HL</p>
      <p><span>Contact us:</span> 01274 569299<br> <a href="mailto:bingley@mavericks80slounge.co.uk">bingley@mavericks80slounge.co.uk</a></p>
    </div>
    <h2>Location</h2>
    <div class="textwidget">
      <div class="flex-video">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2353.649274266977!2d-1.840989784142545!3d53.84910168008747!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487be5a138d1aa47%3A0x135d492579fe6517!2s148+Main+St%2C+Bingley+BD16!5e0!3m2!1sen!2suk!4v1475834866885" style="border:0" allowfullscreen="" frameborder="0" height="450" width="600"></iframe>
      </div>
    </div>
  </div>
  <?php } ?>
  </div>

</div>

<?php
  get_footer();
?>
