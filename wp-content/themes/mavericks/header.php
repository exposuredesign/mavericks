<?php
  //$redirect_link = get_field('redirect_link');
  //if($redirect_link) {
  //  header( 'Location: '.$redirect_link ) ;
 // }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <!--
      // Project: Mavericks 80's Lounge
      // Designer: Para Ltd
      // Design Copyright: Para Ltd
      // Developer: Adam Wadsworth
      // Development Copyright: Adam Wadsworth
    -->
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="initial-scale = 1.0,maximum-scale = 1.0" name="viewport"/>
    <title><?php wp_title(''); ?></title>
    <link href="<?php echo get_template_directory_uri(); ?>/css/style.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/style.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/source/jquery.fancybox.css?v=2.1.5" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]> <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.js"></script> <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script> <![endif]-->
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-84985658-1', 'auto');
  ga('send', 'pageview');

</script>
  <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
    <div class="off-canvas-wrapper">
      <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
        <div class="off-canvas position-right" id="offCanvas" data-off-canvas data-position="right">
          <ul class="vertical menu">
            <li>
              Venues
              <ul class="vertical menu">
                <li><a href="<?php echo get_home_url();?>/huddersfield/huddersfield-venue/">Huddersfield</a></li>
                <li><a href="<?php echo get_home_url();?>/horsforth/horsforth-venue/">Horsforth</a></li>
                <li><a href="<?php echo get_home_url();?>/bingley/bingley-venue/">Bingley</a></li>
              </ul>
            </li>
            <li>
              Events
              <ul class="vertical menu">
                <li><a href="<?php echo get_home_url();?>/events/huddersfield/">Huddersfield</a></li>
                <li><a href="<?php echo get_home_url();?>/events/horsforth/">Horsforth</a></li>
                <li><a href="<?php echo get_home_url();?>/events/bingley/">Bingley</a></li> 
              </ul>
            </li>
            <li>
              Galleries
              <ul class="vertical menu">
                <li><a href="<?php echo get_home_url();?>/huddersfield/huddersfield-gallery/">Huddersfield</a></li>
                <li><a href="<?php echo get_home_url();?>/horsforth/horsforth-gallery/">Horsforth</a></li>
                <li><a href="<?php echo get_home_url();?>/bingley/bingley-gallery/">Bingley</a></li>
              </ul>
            </li>
            <li>
              Book a Booth
              <ul class="vertical menu">
                <li><a href="<?php echo get_home_url();?>/huddersfield/huddersfield-book-booth/">Huddersfield</a></li>
                <li><a href="<?php echo get_home_url();?>/horsforth/horsforth-book-booth/">Horsforth</a></li>
                <li><a href="<?php echo get_home_url();?>/bingley/bingley-book-booth/">Bingley</a></li>
              </ul>
            </li>
            <li>
              <a href="<?php echo get_home_url();?>/mavericks-on-tour/" title="Maverick's on Tour">Maverick's on Tour</a>
            </li>
            <li>
              <a href="<?php echo get_home_url();?>/join-our-team/" title="Join Our Team">Join Our Team</a>
            </li>
          </ul>
        </div>
        <div class="off-canvas-content" data-off-canvas-content>
          <div class="expanded" id="header">
            <div class="one expanded">
              <div class="row">
                <div class="column">
                  <ul class="menu align-right">
                    <li>
                      <a class="facebook" href="https://www.facebook.com/Mavericks80slounge/" target="_blank" title="Facebook">
                        <i aria-hidden="true" class="fa fa-facebook"></i>
                      </a>
                    </li>
                    <li>
                      <a class="facebook" href="https://twitter.com/Mavericks80s" target="_blank" title="Twitter">
                        <i aria-hidden="true" class="fa fa-twitter"></i>
                      </a>
                    </li>
                    <li>
                      <a class="facebook" href="https://www.instagram.com/mavericks80slounge1/" target="_blank" title="Instagram">
                        <i aria-hidden="true" class="fa fa-instagram"></i>
                      </a>
                    </li>
                    <li>
                      <a class="facebook" href="https://www.youtube.com/channel/UCoeeD0T7vKur4zcIJ-n8fPg" target="_blank" title="YouTube">
                        <i aria-hidden="true" class="fa fa-youtube"></i>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>

            <div class="two expanded">
              <div class="row">
                <div class="columns small-12 medium-12 large-3 text-center">
                  <a class="mavericks-logo" href="<?php echo get_home_url();?>" title="Mavericks 80's Lounge">
                    <img alt="Mavericks 80's Lounge" src="<?php echo get_template_directory_uri(); ?>/svgs/mavericks-dark-blue.svg"/>
                  </a>
                </div>
                <div class="columns large-9 show-for-large">
                  <ul class="parent dropdown menu" data-dropdown-menu data-options="disableHover: true; clickOpen: true;">
                    <li>
                      <a href="#" title="Venues">Venues</a>
                      <ul class="menu">
                        <li><a href="<?php echo get_home_url();?>/huddersfield/huddersfield-venue/">Huddersfield</a></li>
                        <li><a href="<?php echo get_home_url();?>/horsforth/horsforth-venue/">Horsforth</a></li>
                        <li><a href="<?php echo get_home_url();?>/bingley/bingley-venue/">Bingley</a></li>
                      </ul>
                    </li>
                    <li>
                      <a href="" title="Events">Events</a>
                      <ul class="menu">
                        <li><a href="<?php echo get_home_url();?>/events/huddersfield/">Huddersfield</a></li>
                        <li><a href="<?php echo get_home_url();?>/events/horsforth/">Horsforth</a></li>
                        <li><a href="<?php echo get_home_url();?>/events/bingley/">Bingley</a></li>
                      </ul>
                    </li>
                    <li>
                      <a href="#" title="Galleries">Galleries</a>
                      <ul class="menu">
                        <li><a href="<?php echo get_home_url();?>/huddersfield/huddersfield-gallery/">Huddersfield</a></li>
                        <li><a href="<?php echo get_home_url();?>/horsforth/horsforth-gallery/">Horsforth</a></li>
                        <li><a href="<?php echo get_home_url();?>/bingley/bingley-gallery/">Bingley</a></li>
                      </ul>
                    </li>
                    <li>
                      <a href="#" title="Book a Booth">Book a Booth</a>
                      <ul class="menu">
                        <li><a href="<?php echo get_home_url();?>/huddersfield/huddersfield-book-booth/">Huddersfield</a></li>
                        <li><a href="<?php echo get_home_url();?>/horsforth/horsforth-book-booth/">Horsforth</a></li>
                        <li><a href="<?php echo get_home_url();?>/bingley/bingley-book-booth/">Bingley</a></li>
                      </ul>
                    </li>
                    <li>
                      <a href="<?php echo get_home_url();?>/mavericks-on-tour/" title="Maverick's on Tour">Maverick's on Tour</a>
                    </li>
                    <li>
                      <a href="<?php echo get_home_url();?>/join-our-team/" title="Join Our Team">Join Our Team</a>
                    </li>
                  </ul>
                </div>
                <div class="columns small-12 medium-12 hide-for-large text-center" style="margin:0px 0px 20px 0px;">
                  <a data-toggle="offCanvas"><i class="fa fa-align-justify" aria-hidden="true"></i> MENU</a>
                </div>
              </div>
            </div>
          </div>
