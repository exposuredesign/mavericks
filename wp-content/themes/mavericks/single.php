<?php
  // Project: Mavericks 80's Lounge
  // Designer: Para Ltd
  // Design Copyright: Para Ltd
  // Developer: Adam Wadsworth
  // Development Copyright: Adam Wadsworth
  get_header();
?>
<!-- Hero -->
<?php $hero = get_field('hero'); if($hero) { ?>
<div class="" id="hero">
  <img alt="" src="<?php echo $hero; ?>" style="width:100%;">
</div>
<?php } ?>
<!-- Main -->
<div class="expanded" id="main">
  <div class="row">
    <div class="columns small-12 medium-8 large-8 posts">
      <h1><?php the_title(); ?></h1>

      <?php if ( have_posts() ) : ?>
      <?php while ( have_posts() ) : the_post(); ?>
      <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' ); $url = $thumb['0']; ?>
      <?php if($url){?>
      <div class="row post">
        <div class="column">
          <a href="<?php echo get_permalink(); ?>" alt="<?php the_title(); ?>" class="thumb" style="margin-bottom:20px !important; float:left; width:100%;">
            <?php echo wp_get_attachment_image(get_post_thumbnail_id($post->ID), 'single'); ?>
          </a>
          <?php the_content(); ?>
        </div>
      </div>
      <?php } else { ?>
        <div class="row post">
          <div class="column">
            <a href="<?php echo get_permalink(); ?>" alt="<?php the_title(); ?>" class="date">
              <span><?php echo get_the_date(); ?></span>
            </a>
            <?php the_content(); ?>
          </div>
        </div>
      <?php }?>
      <?php endwhile; the_posts_pagination(); endif;?>
      <?php wp_reset_query(); ?>

    </div>
    <div class="columns small-12 medium-4 large-4 sidebar">
      <?php foreach ((get_the_category()) as $category) {?>
        <?php $footer = $category->cat_name; if($footer === 'Huddersfield') { ?>
        <?php if ( is_active_sidebar( 'huddersfield' ) ) : ?>
          <?php dynamic_sidebar( 'huddersfield' ); ?>
        <?php endif; ?>
        <?php } ?>
        <?php $footer = $category->cat_name; if($footer === 'Horsforth') { ?>
        <?php if ( is_active_sidebar( 'horsforth' ) ) : ?>
          <?php dynamic_sidebar( 'horsforth' ); ?>
        <?php endif; ?>
        <?php } ?>
        <?php $footer = $category->cat_name; if($footer === 'Bingley') { ?>
        <?php if ( is_active_sidebar( 'bingley' ) ) : ?>
          <?php dynamic_sidebar( 'bingley' ); ?>
        <?php endif; ?>
        <?php } ?>
        <?php $footer = $category->cat_name; if($footer === 'Sowerby-Bridge') { ?>
        <?php if ( is_active_sidebar( 'sowerby-bridge' ) ) : ?>
          <?php dynamic_sidebar( 'sowerby-bridge' ); ?>
        <?php endif; ?>
        <?php } ?>
      <?php }?>
    </div>
</div>
</div>
<!-- Social Footer -->
<div class="expanded" id="social-footer">
  <?php foreach ((get_the_category()) as $category) {?>
    <?php $footer = $category->cat_name; if($footer === 'Huddersfield') { ?>
      <div class="row">
        <div class="columns small-12 medium-4 large-4 one">
          <h4>Facebook Feed
            <a href="">@MavsHuddersfield</a>
          </h4>
          <div>
            <?php dynamic_sidebar( 'facebook' ); ?>
          </div>
        </div>
        <div class="columns small-12 medium-4 large-4 two">
          <h4>Instagram Feed
            <a href="">@MavsHuddersfield</a>
          </h4>
          <div>
            <?php dynamic_sidebar( 'instagram' ); ?>
          </div>
        </div>
        <div class="columns small-12 medium-4 large-4 three">
          <h4>Twitter Feed
            <a href="">@MavsHuddersfield</a>
          </h4>
          <div>
            <?php dynamic_sidebar( 'twitter' ); ?>
          </div>
        </div>
      </div>
    <?php } ?>
    <?php $footer = $category->cat_name; if($footer === 'Horsforth') { ?>
      <div class="row">
        <div class="columns small-12 medium-4 large-4 one">
          <h4>Facebook Feed
            <a href="">@MavsHuddersfield</a>
          </h4>
          <div>
            <?php dynamic_sidebar( 'facebook' ); ?>
          </div>
        </div>
        <div class="columns small-12 medium-4 large-4 two">
          <h4>Instagram Feed
            <a href="">@MavsHuddersfield</a>
          </h4>
          <div>
            <?php dynamic_sidebar( 'instagram' ); ?>
          </div>
        </div>
        <div class="columns small-12 medium-4 large-4 three">
          <h4>Twitter Feed
            <a href="">@MavsHuddersfield</a>
          </h4>
          <div>
            <?php dynamic_sidebar( 'twitter' ); ?>
          </div>
        </div>
      </div>
    <?php } ?>
    <?php $footer = $category->cat_name; if($footer === 'Bingley') { ?>
      <div class="row">
        <div class="columns small-12 medium-4 large-4 one">
          <h4>Facebook Feed
            <a href="">@MavsHuddersfield</a>
          </h4>
          <div>
            <?php dynamic_sidebar( 'facebook' ); ?>
          </div>
        </div>
        <div class="columns small-12 medium-4 large-4 two">
          <h4>Instagram Feed
            <a href="">@MavsHuddersfield</a>
          </h4>
          <div>
            <?php dynamic_sidebar( 'instagram' ); ?>
          </div>
        </div>
        <div class="columns small-12 medium-4 large-4 three">
          <h4>Twitter Feed
            <a href="">@MavsHuddersfield</a>
          </h4>
          <div>
            <?php dynamic_sidebar( 'twitter' ); ?>
          </div>
        </div>
      </div>
    <?php } ?>
    <?php $footer = $category->cat_name; if($footer === 'Sowerby-Bridge') { ?>
      <div class="row">
        <div class="columns small-12 medium-4 large-4 one">
          <h4>Facebook Feed
            <a href="">@MavsHuddersfield</a>
          </h4>
          <div>
            <?php dynamic_sidebar( 'facebook' ); ?>
          </div>
        </div>
        <div class="columns small-12 medium-4 large-4 two">
          <h4>Instagram Feed
            <a href="">@MavsHuddersfield</a>
          </h4>
          <div>
            <?php dynamic_sidebar( 'instagram' ); ?>
          </div>
        </div>
        <div class="columns small-12 medium-4 large-4 three">
          <h4>Twitter Feed
            <a href="">@MavsHuddersfield</a>
          </h4>
          <div>
            <?php dynamic_sidebar( 'twitter' ); ?>
          </div>
        </div>
      </div>
    <?php } ?>
  <?php }?>
</div>


<!-- Signup Footer -->
<div class="expanded" id="signup-footer">
  <div class="row">
    <div class="columns small-12 medium-12 large-6">
      <h5>Stay in the know</h5>
      <p>Sign up to hear about<br/>
        <strong>Mavericks</strong>
        news and offers</p>
    </div>
    <div class="columns small-12 medium-12 large-6">
      <form>
        <div class="row">
          <div class="columns">
            <input placeholder="Enter your email address" type="email">
          </div>
          <div class="shrink columns">
            <button class="button">Subscribe</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- Footer -->
<?php foreach ((get_the_category()) as $category) {?>
  <?php $footer = $category->cat_name; if($footer === 'Huddersfield') { ?>
  <div class="expanded" id="footer">
    <div class="one row">
      <div class="columns small-12 medium-4 large-4">
        <a class="mavericks-logo" href="#" title="Mavericks 80's Lounge"><img alt="Mavericks 80's Lounge" src="<?php echo get_template_directory_uri(); ?>/svgs/mavericks.svg"/></a>
      </div>
      <div class="columns small-12 medium-8 large-8">
        <ul class="menu">
          <li>
            <a href="<?php echo get_home_url();?>/huddersfield/venue/">Venues</a>
          </li>
          <li>
            <a href="<?php echo get_home_url();?>/huddersfield/events/">Events</a>
          </li>
          <li>
            <a href="<?php echo get_home_url();?>/huddersfield/galleries/">Galleries</a>
          </li>
          <li>
            <a href="<?php echo get_home_url();?>/huddersfield/book-a-booth/">Book a Booth</a>
          </li>
          <li>
            <a href="<?php echo get_home_url();?>/mavericks-on-tour/">Mavericks on Tour</a>
          </li>
          <li>
            <a href="<?php echo get_home_url();?>/huddersfield/join-our-team/">Join Our Team</a>
          </li>
        </ul>
      </div>
    </div>
    <div class="two row">
      <div class="column text-center">
        <p>32 King Street, Huddersfield, HD2 2QT
          <span>Contact us:</span>
          01484 531999
          <a href="mailto:huddersfield@mavericks80slounge.co.uk">huddersfield@mavericks80slounge.co.uk</a>
        </p>
        <p>&copy; Copyright <?php echo date("Y") ?>
          <strong>Mavericks</strong>. All rights reserved. Registered in England</p>
        <p>
          <a href="<?php echo get_home_url();?>/privacy-and-cookie-policy/" title="Privacy and Cookie Policy">Privacy and Cookie Policy</a>
          |
          <a href="<?php echo get_home_url();?>/terms-and-conditions/" title="Terms and Conditions">Terms and Conditions</a>
        </p>
      </div>
    </div>
  </div>
  <?php } ?>
  <?php $footer = $category->cat_name; if($footer === 'Horsforth') { ?>
  <div class="expanded" id="footer">
    <div class="one row">
      <div class="columns small-12 medium-4 large-4">
        <a class="mavericks-logo" href="<?php echo get_home_url();?>" title="Mavericks 80's Lounge"><img alt="Mavericks 80's Lounge" src="<?php echo get_template_directory_uri(); ?>/svgs/mavericks.svg"/></a>
      </div>
      <div class="columns small-12 medium-8 large-8">
        <ul class="menu">
          <li>
            <a href="<?php echo get_home_url();?>/horsforth/venue/">Venues</a>
          </li>
          <li>
            <a href="<?php echo get_home_url();?>/horsforth/events/">Events</a>
          </li>
          <li>
            <a href="<?php echo get_home_url();?>/horsforth/galleries/">Galleries</a>
          </li>
          <li>
            <a href="<?php echo get_home_url();?>/horsforth/book-a-booth/">Book a Booth</a>
          </li>
          <li>
            <a href="<?php echo get_home_url();?>/mavericks-on-tour/">Mavericks on Tour</a>
          </li>
          <li>
            <a href="<?php echo get_home_url();?>/horsforth/join-our-team/">Join Our Team</a>
          </li>
        </ul>
      </div>
    </div>
    <div class="two row">
      <div class="column text-center">
        <p>62 Town Street, Horsforth, Leeds LS18 4AP
          <span>Contact us:</span>
          01132 588 599
          <a href="mailto:horsforth@mavericks80slounge.co.uk">horsforth@mavericks80slounge.co.uk</a>
        </p>
        <p>&copy; Copyright <?php echo date("Y") ?>
          <strong>Mavericks</strong>. All rights reserved. Registered in England</p>
        <p>
          <a href="<?php echo get_home_url();?>/privacy-and-cookie-policy/" title="Privacy and Cookie Policy">Privacy and Cookie Policy</a>
          |
          <a href="<?php echo get_home_url();?>/terms-and-conditions/" title="Terms and Conditions">Terms and Conditions</a>
        </p>
      </div>
    </div>
  </div>
  <?php } ?>
  <?php $footer = $category->cat_name; if($footer === 'Bingley') { ?>
  <div class="expanded" id="footer">
    <div class="one row">
      <div class="columns small-12 medium-4 large-4">
        <a class="mavericks-logo" href="<?php echo get_home_url();?>" title="Mavericks 80's Lounge"><img alt="Mavericks 80's Lounge" src="<?php echo get_template_directory_uri(); ?>/svgs/mavericks.svg"/></a>
      </div>
      <div class="columns small-12 medium-8 large-8">
        <ul class="menu">
          <li>
            <a href="<?php echo get_home_url();?>/bingley/venue/">Venues</a>
          </li>
          <li>
            <a href="<?php echo get_home_url();?>/bingley/events/">Events</a>
          </li>
          <li>
            <a href="<?php echo get_home_url();?>/bingley/galleries/">Galleries</a>
          </li>
          <li>
            <a href="<?php echo get_home_url();?>/bingley/book-a-booth/">Book a Booth</a>
          </li>
          <li>
            <a href="<?php echo get_home_url();?>/mavericks-on-tour/">Mavericks on Tour</a>
          </li>
          <li>
            <a href="<?php echo get_home_url();?>/bingley/join-our-team/">Join Our Team</a>
          </li>
        </ul>
      </div> 
    </div>
    <div class="two row">
      <div class="column text-center">
        <p>148 Main Street, Bingley, BD16 2HL
          <span>Contact us:</span>
          01274 569299
          <a href="mailto:bingley@mavericks80slounge.co.uk">bingley@mavericks80slounge.co.uk</a>
        </p>
        <p>&copy; Copyright <?php echo date("Y") ?>
          <strong>Mavericks</strong>. All rights reserved. Registered in England</p>
        <p>
          <a href="<?php echo get_home_url();?>/privacy-and-cookie-policy/" title="Privacy and Cookie Policy">Privacy and Cookie Policy</a>
          |
          <a href="<?php echo get_home_url();?>/terms-and-conditions/" title="Terms and Conditions">Terms and Conditions</a>
        </p>
      </div>
    </div>
  </div>
  <?php } ?>
  <?php $footer = $category->cat_name; if($footer === 'Sowerby-Bridge') { ?>
  <div class="expanded" id="footer">
    <div class="one row">
      <div class="columns small-12 medium-4 large-4">
        <a class="mavericks-logo" href="<?php echo get_home_url();?>" title="Mavericks 80's Lounge"><img alt="Mavericks 80's Lounge" src="<?php echo get_template_directory_uri(); ?>/svgs/mavericks.svg"/></a>
      </div>
    <div class="columns small-12 medium-8 large-8">
      <ul class="menu">
        <li>
          <a href="<?php echo get_home_url();?>/sowerby-bridge//venue/">Venues</a>
        </li>
        <li>
          <a href="<?php echo get_home_url();?>/sowerby-bridge//events/">Events</a>
        </li>
        <li>
          <a href="<?php echo get_home_url();?>/sowerby-bridge//galleries/">Galleries</a>
        </li>
        <li>
          <a href="<?php echo get_home_url();?>/sowerby-bridge//book-a-booth/">Book a Booth</a>
        </li>
        <li>
          <a href="<?php echo get_home_url();?>/sowerby-bridge/-on-tour/">Mavericks on Tour</a>
        </li>
        <li>
          <a href="<?php echo get_home_url();?>/sowerby-bridge//join-our-team/">Join Our Team</a>
        </li>
      </ul>
    </div>
  </div>
    <div class="two row">
      <div class="column text-center">
        <p>1 Canal Basin, Sowerby Bridge, HX6 2AG
          <span>Contact us:</span>
          01422 833940
          <a href="mailti:sowerby-bridge@mavericks80slounge.co.uk">sowerby-bridge@mavericks80slounge.co.uk/a>
        </p>
        <p>&copy; Copyright <?php echo date("Y") ?>
          <strong>Mavericks</strong>. All rights reserved. Registered in England
        </p>
        <p>
          <a href="<?php echo get_home_url();?>/privacy-and-cookie-policy/" title="Privacy and Cookie Policy">Privacy and Cookie Policy</a>
          |
          <a href="<?php echo get_home_url();?>/terms-and-conditions/" title="Terms and Conditions">Terms and Conditions</a>
        </p>
      </div>
    </div>
  </div>
  <?php } ?>
  <?php $footer = $category->cat_name; if($footer === 'Default' || is_page('home')) { ?>
  <div class="expanded" id="footer">
    <div class="one row">
      <div class="column text-center">
        <a class="mavericks-logo" href="<?php echo get_home_url();?>" title="Mavericks 80's Lounge">
          <img alt="Mavericks 80's Lounge" src="<?php echo get_template_directory_uri(); ?>/svgs/mavericks.svg"/>
        </a>
      </div>
    </div>
    <div class="two row">
      <div class="column text-center">
        <p>&copy; Copyright <?php echo date("Y") ?>
          <strong>Mavericks</strong>. All rights reserved. Registered in England</p>
        <p>
          <a href="<?php echo get_home_url();?>/privacy-and-cookie-policy/" title="Privacy and Cookie Policy">Privacy and Cookie Policy</a>
          |
          <a href="<?php echo get_home_url();?>/terms-and-conditions/" title="Terms and Conditions">Terms and Conditions</a>
        </p>
      </div>
    </div>
  </div>
  <?php } ?>
<?php } ?>
<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/slick.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/what-input.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/foundation.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/main.min.js"></script>
</body>
</html>
