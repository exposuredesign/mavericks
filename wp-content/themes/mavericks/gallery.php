<?php
  // Template Name: Gallery
  // Project: Mavericks 80's Lounge
  // Designer: Para Ltd
  // Design Copyright: Para Ltd
  // Developer: Adam Wadsworth
  // Development Copyright: Adam Wadsworth
  get_header();
?>

<?php $hero = get_field('hero'); if($hero) { ?>
<div class="" id="hero">
  <img alt="" src="<?php echo $hero; ?>" style="width:100%;">
</div>
<?php } ?>


<div class="expanded" id="main">
  <div class="expanded">
    <?php $images = get_field('images'); if( $images ) { ?>
          <div class="row">
            <div class="column text-center">
              <h1><?php echo the_title(); ?></h1>
            </div>
          </div>
          <div class="row items">
            <?php foreach( $images as $image ) { ?>
              <div class="columns small-12 medium-4 large-4 item" data-caption="">
                <a href="<?php echo $image['sizes']['large']; ?>" class="fancybox" rel="gallery1" data-title="" style="background: url(<?php echo $image['sizes']['large']; ?>) no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; position: relative;">
                  <span><?php echo $image['caption']; ?></span>
                </a>
              </div>
            <?php } ?>
          </div>
        <?php } else { ?>
        <div class="column text-center">
          <?php $fb_shortcode = get_field('fb_shortcode'); if($fb_shortcode) { ?>
          <?php echo do_shortcode($fb_shortcode); ?>
          <?php } ?>
        </div>
        <?php } ?>
      </div>
  </div>




<?php $type = get_field('type');
if($type == 'Huddersfield') { ?>
<div class="expanded" id="gallery-booth">
  <div class="row">
    <div class="columns small-12 medium-6 large-6">
      <h4 style="color: #110974; font-family: 'reforma'; text-transform: uppercase; font-size: 40px; margin: 0; padding: 0; display: block;">Having a Party?</h4>
      <p>Let us take the headache away from planning your big day, be it birthday celebration, leaving party, or wedding reception – we know how to get a party started. You will have full access to our Party Planner who will take care of your every need, leaving you to sit back, relax and party!!!</p>
    </div>
    <div class="columns small-12 medium-6 large-6 box">
      <div class="book-a-booth" style="background: url(<?php echo home_url(); ?>/wp-content/uploads/2016/09/HuddsBooth-box-bg-762x481.jpg) no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
        <div class="row align-middle">
          <div class="column text-center">
            <span class="title">BOOK A BOOTH</span>
            <span class="content">There’s no party like a Maverick’s party!</span>
            <a href="http://mavericks80slounge.co.uk/huddersfield/huddersfield-book-booth/" class="button">More</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php } ?>
<?php if($type == 'Horsforth') { ?>
  <div class="expanded" id="gallery-booth">
    <div class="row">
      <div class="columns small-12 medium-6 large-6">
        <h4 style="color: #110974; font-family: 'reforma'; text-transform: uppercase; font-size: 40px; margin: 0; padding: 0; display: block;">Having a Party?</h4>
        <p>Let us take the headache away from planning your big day, be it birthday celebration, leaving party, or wedding reception – we know how to get a party started. You will have full access to our Party Planner who will take care of your every need, leaving you to sit back, relax and party!!!</p>
      </div>
      <div class="columns small-12 medium-6 large-6 box">
        <div class="book-a-booth" style="background: url(<?php echo home_url(); ?>/wp-content/uploads/2016/09/HuddsBooth-box-bg-762x481.jpg) no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
          <div class="row align-middle">
            <div class="column text-center">
              <span class="title">BOOK A BOOTH</span>
              <span class="content">There’s no party like a Maverick’s party!</span>
              <a href="http://mavericks80slounge.co.uk/horsforth/horsforth-book-booth/" class="button">More</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php } ?>
<?php if($type == 'Bingley') { ?>
  <div class="expanded" id="gallery-booth">
    <div class="row">
      <div class="columns small-12 medium-6 large-6">
        <h4 style="color: #110974; font-family: 'reforma'; text-transform: uppercase; font-size: 40px; margin: 0; padding: 0; display: block;">Having a Party?</h4>
        <p>Let us take the headache away from planning your big day, be it birthday celebration, leaving party, or wedding reception – we know how to get a party started. You will have full access to our Party Planner who will take care of your every need, leaving you to sit back, relax and party!!!</p>
      </div>
      <div class="columns small-12 medium-6 large-6 box">
        <div class="book-a-booth" style="background: url(<?php echo home_url(); ?>/wp-content/uploads/2016/09/HuddsBooth-box-bg-762x481.jpg) no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
          <div class="row align-middle">
            <div class="column text-center">
              <span class="title">BOOK A BOOTH</span>
              <span class="content">There’s no party like a Maverick’s party!</span>
              <a href="http://mavericks80slounge.co.uk/bingley/bingley-book-booth/" class="button">More</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php } ?>

</div>
<?php
  get_footer();
?>
