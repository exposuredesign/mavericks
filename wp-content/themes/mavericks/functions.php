<?php
  // Project: Mavericks 80's Lounge
  // Designer: Para Ltd
  // Design Copyright: Para Ltd
  // Developer: 
  // Development Copyright: 


  add_action('get_header', 'remove_admin_login_header');
  function remove_admin_login_header() {
	   remove_action('wp_head', '_admin_bar_bump_cb');
   }

  /* Menu Support */
  function menu_support() {
    register_nav_menus(
    array(
      'venues-menu' => __( 'Venues' ),
      'events-menu' => __( 'Events' ),
      'galleries-menu' => __( 'Galleries' ),
      'book-a-booth-menu' => __( 'Book a Booth' ),
      'mavericks-on-tour-menu' => __( 'Mavericks on Tour' ),
      'join-our-team-menu' => __( 'Join Our Team' )
      )
    );
  }
  add_action( 'init', 'menu_support' );

  /* Sidebar Support */
  function sidebar_support() {

  	register_sidebar( array(
  		'name' => __( 'Huddersfield Facebook', 'huddersfield-f' ),
  		'id' => 'huddersfield-f',
  		'before_widget' => '',
  		'after_widget' => '',
  		'before_title' => '<h2>',
  		'after_title' => '</h2>'
  	) );
    register_sidebar( array(
  		'name' => __( 'Horsforth Facebook', 'horsforth-f' ),
  		'id' => 'horsforth-f',
      'before_widget' => '',
  		'after_widget' => '',
  		'before_title' => '<h2>',
  		'after_title' => '</h2>'
  	) );
    register_sidebar( array(
      'name' => __( 'Bingley Facebook', 'bingley-f' ),
      'id' => 'bingley-f',
      'before_widget' => '',
  		'after_widget' => '',
  		'before_title' => '<h2>',
  		'after_title' => '</h2>'
    ) );
    register_sidebar( array(
      'name' => __( 'Default Facebook', 'facebook' ),
      'id' => 'facebook',
      'before_widget' => '',
      'after_widget' => '',
      'before_title' => '<h2>',
      'after_title' => '</h2>'
    ) );
    register_sidebar( array(
      'name' => __( 'Default Twitter', 'twitter' ),
      'id' => 'twitter',
      'before_widget' => '',
      'after_widget' => '',
      'before_title' => '<h2>',
      'after_title' => '</h2>'
    ) );
    register_sidebar( array(
      'name' => __( 'Default Instagram', 'instagram' ),
      'id' => 'instagram',
      'before_widget' => '',
      'after_widget' => '',
      'before_title' => '<h2>',
      'after_title' => '</h2>'
    ) );
  }
  add_action( 'widgets_init', 'sidebar_support' );

  /* Featured Image Support */
  add_theme_support( 'post-thumbnails' );
  add_image_size( 'single', 652, 320, true );

?>
