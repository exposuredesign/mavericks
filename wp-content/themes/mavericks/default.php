<?php
  // Template Name: Default
  // Project: Mavericks 80's Lounge
  // Designer: Para Ltd
  // Design Copyright: Para Ltd
  // Developer: Adam Wadsworth
  // Development Copyright: Adam Wadsworth
  get_header();
?>
<!-- Hero -->
<?php $hero = get_field('hero'); if($hero) { ?>
<div class="" id="hero">
  <img alt="" src="<?php echo $hero; ?>" style="width:100%;">
</div>
<?php } ?>
<!-- Main -->
<?php while ( have_posts() ) : the_post(); ?>
<div class="expanded" id="main">
  <div class="row">
    <div class="columns small-12 medium-12 large-12">
      <h1><?php the_title(); ?></h1>
      <?php the_content(); ?>
    </div>
  </div>
</div>
<?php endwhile;?>
<?php wp_reset_query(); ?>
<?php
  get_footer();
?>
