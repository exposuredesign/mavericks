      <?php
        $type = get_field('type');
        $queried_object = get_queried_object();
        $taxonomy = $queried_object->taxonomy;
        $term_id = $queried_object->term_id;
        $cat_type = get_field('type', $queried_object);
       ?>
       <!-- Social Footer -->
       <div class="expanded" id="social-footer">
         <div class="row">
           <div class="columns small-12 medium-4 large-4 one">
             <h4>Facebook Feed</h4>
             <div>
               <?php if($type == 'Huddersfield' || $cat_type == 'Huddersfield') { ?>
                 <?php dynamic_sidebar( 'huddersfield-f' ); ?>
               <?php } ?>

               <?php if($type == 'Horsforth' || $cat_type == 'Horsforth' ) { ?>
                 <?php dynamic_sidebar( 'horsforth-f' ); ?>
               <?php } ?>

               <?php if($type == 'Bingley' || $cat_type == 'Bingley') { ?>
                 <?php dynamic_sidebar( 'bingley-f' ); ?>
               <?php } ?>

               <?php if($type != 'Huddersfield' && $type != 'Horsforth' && $type != 'Bingley' && $cat_type != 'Huddersfield' && $cat_type != 'Horsforth' && $cat_type != 'Bingley') { ?>
                 <?php dynamic_sidebar( 'facebook' ); ?>
               <?php } ?>
             </div>
           </div>
           <div class="columns small-12 medium-4 large-4 two">
             <h4>Instagram Feed</h4>
             <div>
               <?php dynamic_sidebar( 'instagram' ); ?>
             </div>
           </div>
           <div class="columns small-12 medium-4 large-4 three">
             <h4>Twitter Feed</h4>
             <div>
               <?php dynamic_sidebar( 'twitter' ); ?>
             </div>
           </div>
         </div>
       </div>


       <?php if(is_page_template('home.php')) { ?>
         <div class="expanded" id="additional-content-footer">
           <div class="row">
             <div class="column text-center">
               <?php $value = get_field( "additional_content" ); echo $value; ?>
             </div>
           </div>
         </div>
       <?php } ?>

       <?php if($type == 'Huddersfield' && is_page_template('venue.php') || $cat_type == 'Huddersfield' && is_page_template('venue.php')) { ?>
         <div class="expanded" id="additional-content-footer">
           <div class="row">
             <div class="column text-center">
               <?php the_field('additional_content', 'category_3'); ?>
             </div>
           </div>
         </div>
       <?php } ?>

       <?php if($type == 'Horsforth' && is_page_template('venue.php') || $cat_type == 'Horsforth' && is_page_template('venue.php')) { ?>
         <div class="expanded" id="additional-content-footer">
           <div class="row">
             <div class="column text-center">
               <?php the_field('additional_content', 'category_4'); ?>
             </div>
           </div>
         </div>
       <?php } ?>

       <?php if($type == 'Bingley' && is_page_template('venue.php') || $cat_type == 'Bingley' && is_page_template('venue.php')) { ?>
         <div class="expanded" id="additional-content-footer">
           <div class="row">
             <div class="column text-center">
               <?php the_field('additional_content', 'category_5'); ?>
             </div>
           </div>
         </div>
       <?php } ?>


       <!-- Credits -->
      <?php if($type == 'Huddersfield' || $cat_type == 'Huddersfield') { ?>
        <!-- MailChimp -->
        <div class="expanded" id="signup-footer">
          <div class="row">
            <div class="columns small-12 medium-12 large-6 text-center medium-text-center large-text-left">
              <h5>Stay in the know</h5>
              <p>Sign up to hear about<br><strong>Mavericks</strong> news and offers</p>
            </div>
            <div class="columns small-12 medium-12 large-6">
              <?php echo do_shortcode('[mc4wp_form id="392"]');?>
            </div>
          </div>
        </div>
      <div class="expanded" id="footer">
        <div class="one row">
          <div class="columns small-12 medium-12 large-4 text-center medium-text-center large-text-left">
            <a class="mavericks-logo" href="<?php echo get_home_url();?>" title="Mavericks 80's Lounge">
              <img alt="Mavericks 80's Lounge" src="<?php echo get_template_directory_uri(); ?>/svgs/mavericks-light-blue.svg">
            </a>
          </div>
          <div class="columns small-12 medium-8 large-8 show-for-large">
            <ul class="menu">
              <li><a href="<?php echo get_home_url();?>/huddersfield/huddersfield-venue/">Venues</a></li>
              <li><a href="<?php echo get_home_url();?>/huddersfield/huddersfield-events/">Events</a></li>
              <li><a href="<?php echo get_home_url();?>/huddersfield/huddersfield-gallery/">Galleries</a></li>
              <li><a href="<?php echo get_home_url();?>/huddersfield/huddersfield-book-booth/">Book a Booth</a></li>
              <li><a href="<?php echo get_home_url();?>/mavericks-on-tour/">Mavericks on Tour</a></li>
              <li><a href="<?php echo get_home_url();?>/join-our-team/">Join Our Team</a></li>
            </ul>
          </div>
        </div>
        <div class="two row">
          <div class="column text-center">
            <p>32 King Street, Huddersfield, HD2 2QT
              <span>Contact us:</span> 01484 531999 <a href="mailto:huddersfield@mavericks80slounge.co.uk">huddersfield@mavericks80slounge.co.uk</a>
            </p>
            <p>&copy; Copyright <?php echo date("Y"); ?> <strong>Maverick's</strong>. All rights reserved. Registered in England</p>
            <p><a href="<?php echo get_home_url();?>/privacy-cookie-policy/" title="Privacy and Cookie Policy">Privacy and Cookie Policy</a> | <a href="<?php echo get_home_url();?>/terms-conditions" title="Terms and Conditions">Terms and Conditions</a></p>
          </div>
        </div>
      </div>
      <?php } ?>
      <?php if($type == 'Horsforth' || $cat_type == 'Horsforth') { ?>
        <!-- MailChimp -->
        <div class="expanded" id="signup-footer">
          <div class="row">
            <div class="columns small-12 medium-12 large-6 text-center medium-text-center large-text-left">
              <h5>Stay in the know</h5>
              <p>Sign up to hear about<br><strong>Mavericks</strong> news and offers</p>
            </div>
            <div class="columns small-12 medium-12 large-6">
              <?php echo do_shortcode('[mc4wp_form id="392"]');?>
            </div>
          </div>
        </div>
      <div class="expanded" id="footer">
        <div class="one row">
          <div class="columns small-12 medium-12 large-4 text-center medium-text-center large-text-left">
            <a class="mavericks-logo" href="<?php echo get_home_url();?>" title="Mavericks 80's Lounge">
              <img alt="Mavericks 80's Lounge" src="<?php echo get_template_directory_uri(); ?>/svgs/mavericks-light-blue.svg">
            </a>
          </div>
          <div class="columns small-12 medium-8 large-8 show-for-large">
            <ul class="menu">
              <li><a href="<?php echo get_home_url();?>/horsforth/horsforth-venue/">Venues</a></li>
              <li><a href="<?php echo get_home_url();?>/horsforth/horsforth-events/">Events</a></li>
              <li><a href="<?php echo get_home_url();?>/horsforth/horsforth-gallery/">Galleries</a></li>
              <li><a href="<?php echo get_home_url();?>/horsforth/horsforth-book-booth/">Book a Booth</a></li>
              <li><a href="<?php echo get_home_url();?>/mavericks-on-tour/">Mavericks on Tour</a></li>
              <li><a href="<?php echo get_home_url();?>/join-our-team/">Join Our Team</a></li>
            </ul>
          </div>
        </div>
        <div class="two row">
          <div class="column text-center">
            <p>62 Town Street, Horsforth, Leeds LS18 4AP
              <span>Contact us:</span> 01132 588 599 <a href="mailto:horsforth@mavericks80slounge.co.uk">horsforth@mavericks80slounge.co.uk</a>
            </p>
            <p>&copy; Copyright <?php echo date("Y"); ?> <strong>Maverick's</strong>. All rights reserved. Registered in England</p>
            <p><a href="<?php echo get_home_url();?>/privacy-cookie-policy/" title="Privacy and Cookie Policy">Privacy and Cookie Policy</a> | <a href="<?php echo get_home_url();?>/terms-conditions" title="Terms and Conditions">Terms and Conditions</a></p>
          </div>
        </div>
      </div>
      <?php } ?>
      <?php if($type == 'Bingley' || $cat_type == 'Bingley') { ?>
        <!-- MailChimp -->
        <div class="expanded" id="signup-footer">
          <div class="row">
            <div class="columns small-12 medium-12 large-6 text-center medium-text-center large-text-left">
              <h5>Stay in the know</h5>
              <p>Sign up to hear about<br><strong>Mavericks</strong> news and offers</p>
            </div>
            <div class="columns small-12 medium-12 large-6">
              <?php echo do_shortcode('[mc4wp_form id="392"]');?>
            </div>
          </div>
        </div>
      <div class="expanded" id="footer">
        <div class="one row">
          <div class="columns small-12 medium-12 large-4 text-center medium-text-center large-text-left">
            <a class="mavericks-logo" href="<?php echo get_home_url();?>" title="Mavericks 80's Lounge">
              <img alt="Mavericks 80's Lounge" src="<?php echo get_template_directory_uri(); ?>/svgs/mavericks-light-blue.svg">
            </a>
          </div>
          <div class="columns small-12 medium-8 large-8 show-for-large">
            <ul class="menu">
              <li><a href="<?php echo get_home_url();?>/bingley/bingley-venue/">Venues</a></li>
              <li><a href="<?php echo get_home_url();?>/bingley/bingley-events/">Events</a></li>
              <li><a href="<?php echo get_home_url();?>/bingley/bingley-gallery/">Galleries</a></li>
              <li><a href="<?php echo get_home_url();?>/bingley/bingley-book-booth/">Book a Booth</a></li>
              <li><a href="<?php echo get_home_url();?>/mavericks-on-tour/">Mavericks on Tour</a></li>
              <li><a href="<?php echo get_home_url();?>/join-our-team/">Join Our Team</a></li>
            </ul>
          </div>
        </div>
        <div class="two row">
          <div class="column text-center">
            <p>148 Main Street, Bingley, BD16 2HL
              <span>Contact us:</span> 01274 569299 <a href="mailto:bingley@mavericks80slounge.co.uk">bingley@mavericks80slounge.co.uk</a>
            </p>
            <p>&copy; Copyright <?php echo date("Y"); ?> <strong>Maverick's</strong>. All rights reserved. Registered in England</p>
            <p><a href="<?php echo get_home_url();?>/privacy-cookie-policy/" title="Privacy and Cookie Policy">Privacy and Cookie Policy</a> | <a href="<?php echo get_home_url();?>/terms-conditions" title="Terms and Conditions">Terms and Conditions</a></p>
          </div>
        </div>
      </div>
      <?php } ?>
      <?php if($type != 'Huddersfield' && $type != 'Horsforth' && $type != 'Bingley' && $cat_type != 'Huddersfield' && $cat_type != 'Horsforth' && $cat_type != 'Bingley') { ?>
      <div class="expanded" id="footer">
        <div class="one row">
          <div class="column text-center">
            <a class="mavericks-logo" href="<?php echo get_home_url();?>" title="Mavericks 80's Lounge">
              <img alt="Mavericks 80's Lounge" src="<?php echo get_template_directory_uri(); ?>/svgs/mavericks-light-blue.svg">
            </a>
          </div>
        </div>
        <div class="two row">
          <div class="column text-center">
            <p>&copy; Copyright <?php echo date("Y"); ?> <strong>Maverick's</strong>. All rights reserved. Registered in England</p>
            <p><a href="<?php echo get_home_url();?>/privacy-cookie-policy/" title="Privacy and Cookie Policy">Privacy and Cookie Policy</a> | <a href="<?php echo get_home_url();?>/terms-conditions" title="Terms and Conditions">Terms and Conditions</a></p>
          </div>
        </div>
      </div>
      <?php } ?>


      </div>
    </div>
  </div>

  <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/jquery.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/slick.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/what-input.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/foundation.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/js/main.min.js"></script>

  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/lib/jquery.mousewheel-3.0.6.pack.js"></script>
  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/source/jquery.fancybox.js?v=2.1.5"></script>
  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
</body>
</html>
