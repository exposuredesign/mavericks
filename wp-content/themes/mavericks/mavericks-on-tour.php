<?php
  // Template Name: Mavericks on Tour
  // Project: Mavericks 80's Lounge
  // Designer: Para Ltd
  // Design Copyright: Para Ltd
  // Developer: Adam Wadsworth
  // Development Copyright: Adam Wadsworth
  get_header();
?>

<?php $hero = get_field('hero'); if($hero) { ?>
<div class="" id="hero">
  <img alt="" src="<?php echo $hero; ?>" style="width:100%;">
</div>
<?php } ?>

<div class="expanded" id="main">

  <?php if( have_rows('block') ): ?>
    <?php while( have_rows('block') ): the_row(); $title = get_sub_field('title'); $content = get_sub_field('content');?>
      <div class="row">
        <div class="column text-center content">
          <h1 style="margin-left:0px; padding-left:0px;"><?php echo $title; ?></h1>
          <p><?php echo $content; ?></p>
        </div>
      </div>
    <?php endwhile; ?>
  <?php endif; ?>

  <?php if( have_rows('blocks') ): ?>
    <div class="row items" data-equalizer>
      <?php while( have_rows('blocks') ): the_row();
        $title = get_sub_field('title');
        $content = get_sub_field('content');
        $gallery_link = get_sub_field('gallery_link');
        $youtube_url = get_sub_field('youtube_url');
        $background = get_sub_field('background');
      ?>
        <div class="columns small-12 medium-6 large-6 item">
          <div class="text-center" data-equalizer-watch style="background: url(<?php echo $background; ?>) no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
            <h2><?php echo $title;?></h2>
            <p style="margin-bottom:15px;">
              <span><?php echo $content; ?></span>
            </p>
            <p></p>
            <?php if($gallery_link){ ?><a class="button" href="<?php echo $gallery_link;?>">View Gallery</a><?php } ?>
            <?php if($youtube_url){ ?><a class="button fancybox-media" href="<?php echo $youtube_url; ?>">View Video</a><?php } ?>
          </div>
        </div>
      <?php endwhile; ?>
    </div>
  <?php endif; ?>

  <?php if( have_rows('banner') ): ?>
    <?php while( have_rows('banner') ): the_row();
      $title = get_sub_field('title');
      $content = get_sub_field('content');
      $background = get_sub_field('background');
    ?>
      <div class="row">
        <div class="column box text-center" style="background: url(<?php echo $background; ?>) no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
          <h3 class="text-center"><?php echo $title; ?></h3>
          <p class="text-center"><?php echo $content; ?></p>
        </div>
      </div>
    <?php endwhile; ?>
  <?php endif; ?>

</div>

<?php
  get_footer();
?>
