<?php
//SrizonResourceLoader::load_mag_popup();
//SrizonResourceLoader::load_collage_plus();
//SrizonResourceLoader::load_srizon_custom_css();
$url = remove_query_arg($aid);
$url = remove_query_arg($paging_id,$url);
if($srz_common_options['jumptoarea'] == 'true'){
	$blink = $url.'#'.$scroller_id;
}
else{
	$blink = $url;
}
$extraclass = '';
if($srz_page['showhoverzoom']) $extraclass.=' zoom';
$backtogallerytxt = $srz_page['backtogallerytxt'];
if(empty($backtogallerytxt)) {
	$backtogallerytxt = $srz_common_options['backtogallerytxt'];
}
//$backlink = ' <a href="' . $blink . '">' .  __($backtogallerytxt,'srizon-facebook-album') . '</a>';
$dtg=' data-gallery="gallery"';
if ($set) {
	$data .= '<h1>'.filter_fb_text($pagetitle).'</h1>';
	$dtg='';
} else {
	$data .= '<h1>Galleries</h1>';
	$dtg='';
}

$data .= '<div class="fbalbum'.$extraclass.'"  id="' . $scroller_id . '"><div class="row items">';
foreach ($srz_images as $image) {
	if ($set) {
		$link = $image['src'];
		$grelval = $lightbox_attribute;
	} else {
		$u = remove_query_arg($paging_id);
		$link = add_query_arg($aid,$image['id'],$u);
		if($srz_common_options['jumptoarea'] == 'true'){
			$link = $link.'#'.$scroller_id;
		}
		$grelval = '';
		if(! empty($srz_page['albumtxt'])) {
			$image['txt'] = __($srz_page['albumtxt'] . ': ', 'srizon-facebook-album') . filter_fb_text($image['txt']) . "\n";
		} elseif(! empty($srz_common_options['albumtxt'])) {
			$image['txt'] = __($srz_common_options['albumtxt'] . ': ', 'srizon-facebook-album') . filter_fb_text($image['txt']) . "\n";
		} else {
			$image['txt'] = filter_fb_text($image['txt']) . "\n";
		}
		if ($srz_page['show_image_count']) {
			$image['txt'] = $image['txt'] . $image['count'] . __(' Photos','srizon-facebook-album');
		}
	}
	$caption = nl2br($image['txt']);
	$data .= <<<EOL
		<div class="columns small-12 medium-4 large-4 item" data-caption="{$caption}">
			<a href="{$link}" class="fancybox" rel="gallery1" data-title="{$caption}" {$grelval}{$dtg} style="background: url({$image['thumb']}) no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; position: relative;">
				<span>{$caption}</span>
			</a>
		</div>
EOL;
}
$data .= '</div></div>';
if($set) {
	$addcaption = ( $srz_page['hovercaption'] ) ? '.collageCaption({behaviour_c: ' . $srz_page['hovercaptiontype'] . '})' : '';
}
else{
	$addcaption = ( $srz_page['hovercaption'] ) ? '.collageCaption({behaviour_c: ' . $srz_page['hovercaptiontypecover'] . '})' : '';
}

$data .= <<<EOL
<script>
	;
	jQuery(document).ready(function(){
		jQuery('#{$scroller_id} .Image_Wrapper').css("opacity", 0.3);
		jQuery('#{$scroller_id}').removeWhitespace().collagePlus({
			'allowPartialLastRow': {$srz_page['collagepartiallast']},
			'targetHeight': {$srz_page['maxheight']},
			'padding': {$srz_page['collagepadding']}
		}){$addcaption};
	});
	jQuery(window).resize(function(){
		jQuery('#{$scroller_id}').collagePlus({
			'allowPartialLastRow': {$srz_page['collagepartiallast']},
			'targetHeight': {$srz_page['maxheight']},
			'padding': {$srz_page['collagepadding']}
		});
	});
</script>
EOL;
